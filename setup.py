import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="reddit_base_bot",
    version="0.0.1",
    author="Björn Jürgens",
    author_email="author@example.com",
    description="A small base for making simple reddit bots",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/juergens/reddit_bot_base",
    packages=setuptools.find_packages(),
    install_requires=['praw'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)